# fam: flag-and-move

If you want to move your files and directories but don't know yet the destination, `fam` (flag-and-move) lets you flag them and then move them to your current working directory.

## Installation

```bash
git clone https://gitlab.com/montazar/fam.git
cd fam
sudo cp fam /usr/local/bin
chmod 644 _fam

# Bash/ZSH completion
if ! [ -d "$ZSH/completions" ]; then
	mkdir "$ZSH/completions"
fi

cp _fam "$ZSH/completions"
sudo cp _fam /usr/share/bash-completion/completions
```

## Manual

You flag files and directories (“items”) by running `fam` without any options, passing each item as an argument. For example, if you want to flag everything in your current working directory:

```bash
fam *
```

You can list flagged files and directories using the `--list` (or shorthand `-l` ) option. Each item is associated with an index. The index is used for selecting the item you want to move to the current working directory using the `-p` option.

```bash
fam -p 5
```

 If you omit the index, the most recently flagged item will be moved to the current working directory. That is, index 1.

```bash
fam -p
```

The moved item(s) will be removed from the list.

You can clear the list by using the  `--clear` option (or shorthand `-C`), or if you want to unflag a specific item in the list, use the `-d INDEX` option.

```bash
fam -d 5
```

You can also unflag from a **range** using the same option.

```bash
fam -d 5-10
```

Unflags all items from the 5th index to the 10th (inclusive).

```bash
fam -d 5-
```

Unflags all items, starting from the 5th index and higher.



```
FAM(1)                                                                                User Commands

NAME
       fam - flag files and directories for moving

SYNOPSIS
       fam [OPTION] [FILE...]

DESCRIPTION
       Flag any files and directories and move to current working directory.

OPTIONS
       -l, --list
              list all flagged

       -a, --all
              move all flagged to current working directory

       -p [INDEX|RANGE]
              move recently flagged, a specific item in the list or all items within a range

       -d [INDEX|RANGE]
              unflag recently flagged, a specific item from the list or all items within a range

       -C, --clear
              clear all flagged

       -h, --help
              print this manual

EXAMPLES
       fam *  
       		  flag everything in current working directory for move

       fam -p 
       		  move recently flagged to current working directory

       fam -p 5
              move the 5th entry in the list

       fam -p 7-9
              move all items from the 7th to 9th (inclusive) index

       fam --all
              move all flagged files and directories

       fam -d 7-
              unflag all items from the 7th index and higher (inclusive)

fam 1.0.1                                                                             November 2020

```

