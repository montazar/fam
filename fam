#!/usr/bin/env bash

########################
#### MAGIC COMMANDS ####
########################

mv="mv -f --strip-trailing-slashes"

########################
### GLOBAL VARIABLES ###
########################

savepoint="/tmp/fam-savepoint-$(whoami).txt"

if ! [ -f "$savepoint" ]; then
    if ! touch "$savepoint"; then
        echo 'Could not create save file.'
        echo 'Exiting...'
        exit 1
    fi
else
    sed -i '/^$/d' "$savepoint"
fi

lines=$(wc -l < "$savepoint")

exit-if-list-empty() {
    if [ "$lines" -lt 1 ]; then
        echo 'List is empty.'
        exit 0
    fi
}

flag() {
    for item in $(realpath $1); do
        if [ -e "$item" ] && ! grep -q  "$item" "$savepoint" ; then
            echo "$item" >> "$savepoint"
        fi
    done
}

move() {
    exit-if-list-empty
    
    local position="$1"
    position=$(((lines + 1) - position))

    local item="$(sed -n "${position}p" "$savepoint")"

    if [ -f "$item" ] || [ -d "$item" ]; then
        $mv "$item" "$(pwd)"

    else
        echo "Ignoring $item..."
    fi

    sed -i "${position}d" "$savepoint"
}

move-range() {
    exit-if-list-empty
    
    local start=$(echo "$1" | awk -F- '{print $1}')
    local end=$(echo "$1" | awk -F- '{print $2}')

    if [ "$start" -gt 0 ] && [ "$end" -le "$lines" ] && [ "$start" -le "$end" ]; then
        while [ "$end" -ge "$start" ]; do
            move "$start" 
            start=$((start + 1))
        done
    fi
}

delete() {
    exit-if-list-empty

    local position="$1"

    if [ -z "$position" ]; then
        echo "Expected an index between 1-$lines, got ($position)"
        exit 1
    fi

    position=$(((lines + 1) - position))
    sed -i "${position}d" "$savepoint"
}

delete-range() {
    exit-if-list-empty

    local start=$(echo "$1" | awk -F- '{print $1}')
    local end=$(echo "$1" | awk -F- '{print $2}')

    if [ "$start" -gt 0 ] && [ "$end" -le "$lines" ] && [ "$start" -le "$end" ]; then
        start=$(((lines + 1) - start))
        end=$(((lines + 1) - end))

        sed -i "${end},${start}d" "$savepoint"
    fi
}

list-items() {
    exit-if-list-empty

    local i="$(wc -l < "$savepoint")" 
    
    for item in $(cat "$savepoint"); do
        echo "$i $item"
        i=$((i - 1))
    done
}


clear-list() {
    echo "" > "$savepoint"
}

usage() {
    echo "Usage: fam [OPTION] [FILE...]"
    echo
    echo "Flag any files and directories and move to current working directory."
    echo
    echo "Options:"
    echo
    echo " -l, --list           list all flagged"
    echo " -a, --all            move all flagged to current working directory"
    echo " -p [INDEX|RANGE]     move recently flagged, a specific item in the list or all items within a range"
    echo " -d [INDEX|RANGE]     unflag recently flagged, a specific item from the list or all items within a range"
    echo " -C, --clear          clear all flagged"
    echo " -h, --help           print this manual"
    echo
    echo "Examples:"
    echo
    echo " fam *                flag everything in current working directory for move"
    echo " fam -p               move recently flagged to current working directory"
    echo " fam -p 5             move the 5th entry in the list"
    echo " fam -p 7-9           move all items from the 7th to 9th (inclusive) index"
    echo " fam --all            move all flagged files and directories"
    echo " fam -d 7-            unflag all items from the 7th index and higher (inclusive)"
}

version() {
    echo 'fam 1.0.1'
}

options=$(getopt -n "fam" -o ald::p::hC --long all,clear,list,help,version -- "$@")
eval set -- "$options"
while :; do
    case $1 in
    -a|--all)
        move-range "1-$lines"
        clear-list
        exit 0
        ;;
	-l|--list)
        list-items
        exit 0
	    ;;
    -d)
        shift
        if echo "$3" | grep -qE '^[0-9]+\-[0-9]+$'; then
            delete-range "$3"
        elif echo "$3" | grep -qE '^([0-9]+\-)$'; then
            delete-range "$3$lines"
        elif echo "$3" | grep -qE '^[0-9]+$'; then
            delete "$3"
        else
            delete 1
        fi
        exit 0
        ;;
	-p)
        shift
        if echo "$3" | grep -qE '^[0-9]+\-[0-9]+$'; then
            move-range "$3"
        elif echo "$3" | grep -qE '^([0-9]+\-)$'; then
            move-range "$3$lines"
        elif echo "$3" | grep -qE '^[0-9]+$'; then
            move "$3"
        else
            move 1
        fi
        exit 0
	    ;;
	-h|--help)
	    usage
	    exit 0
	    ;;
    -C|--clear)
        clear-list
        exit 0
        ;;
    --version)
        version
        exit 0
        ;;
    --)
        shift
        if [ -z "$*" ]; then
            move "1"
        else
            flag "$*"
        fi
        exit 0
        ;;
	*)
	    printf "Unknown option: (%s).\n" "$1"
        usage
        exit 1
	    ;;
    esac
done
eval set -- "$@"
